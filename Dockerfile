FROM node:12

RUN mkdir -p /src && chown -R node:node /src

WORKDIR /src

COPY package*.json ./

RUN npm install

COPY . .

COPY --chown=node:node . .

USER node

EXPOSE 8080

CMD ["node", "src/server.js"]
