const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const {performance} = require('perf_hooks');

const UserController = require('./controller/userController');
const UniController = require('./controller/uniController');
const LectController = require('./controller/lectController');
const RateController = require('./controller/rateController');

const app = express();

console.log('[Server] Initialize MongoDB Connection');
let dbConectionTime = performance.now();
mongoose.connect(
    'mongodb+srv://gorbaczow:michael96@us-projekt-yxy4c.mongodb.net/test?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
    .then(() => console.log('[Server] Successfully connected to MongoDB in ' + Math.ceil(performance.now() - dbConectionTime) + 'ms'))
    .catch(error => console.log(error.message));


app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin,X-Request-With,Content-Type,Accept,Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT,POST,PATCH,DELETE,GET');
        return res.status(200).json({});
    }
    next();
});

//Controllers
app.use('/api/auth', UserController);
app.use('/api/uni', UniController);
app.use('/api/lect', LectController);
app.use('/api/rate', RateController);

app.use((req, res, next) => {
    const error = new Error('Page Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message,
            status: error.status
        }
    });
});

module.exports = app;
