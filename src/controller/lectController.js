const express = require('express');
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

const lectService = require('../service/lectService');

router.get('/byUni/:uniId',  lectService.getAllLect);

router.get('/:lectId', lectService.getLect);


router.post('/', checkAuth,  lectService.addRequest);

router.patch('/:lectId', lectService.patchSummaryRate);

module.exports = router;
