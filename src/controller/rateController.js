const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');
const rateMiddleware = require('../middleware/rateMiddleware');

const rateService = require('../service/rateService');

router.get('/:lectId', rateService.getRates);

router.post('', checkAuth, rateService.addRate);

router.delete('/:_id', rateMiddleware, rateService.deleteRate);

router.get('/:userId/:commentId', rateService.checkUserId);

router.get('/check/:lectId/:userId', rateService.isDoubledComment);

module.exports = router;
