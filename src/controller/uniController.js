const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

const uniService = require('../service/uniService');

router.get('', uniService.getAllUnis);

router.get('/topId', uniService.getTopUnisId);

router.get('/fav/:userId', checkAuth, uniService.getFavUnis);

router.patch('/fav/:userId', uniService.setFavUnis);

module.exports = router;
