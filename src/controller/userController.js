const express = require('express');
const router = express.Router();

const userService = require('../service/userService');

router.post('/signup', userService.signUp);

router.post('/login', userService.logIn);

module.exports = router;
