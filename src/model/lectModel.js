const mongoose = require('mongoose');

const lectSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        require: true,
        maxLength: 100
    },
    surname: {
        type: String,
        require: true,
        maxLength: 200
    },
    faculty: {
        type: String,
        require: true,
        maxLength: 300,
    },
    summaryRate: {
        type: Number,
        require: true,
    },
    lectureRate: {
        type: Number,
        require: true,
    },
    competenceRate: {
        type: Number,
        require: true,
    },
    passRate: {
        type: Number,
        require: true,
    },
    proStudentRate: {
        type: Number,
        require: true,
    },
    activitiesRate: {
        type: Number,
        require: true,
    },
    uniId: {
        type: String,
        require: true
    }
});

module.exports = mongoose.model('Lect', lectSchema);
