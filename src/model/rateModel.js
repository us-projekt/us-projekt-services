const mongoose = require('mongoose');

const rateSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    author: {
        type: String,
        require: true,
        maxLength: 200
    },
    userId: {
        type: String,
        require: true,
    },
    description: {
        type: String,
        require: true,
        maxLength: 300
    },
    date: {
        type: String,
        require: true,
        maxLength: 100,
    },
    summaryRate: {
        type: Number,
        require: true,
    },
    lectureRate: {
        type: Number,
        require: true,
    },
    competenceRate: {
        type: Number,
        require: true,
    },
    passRate: {
        type: Number,
        require: true,
    },
    proStudentRate: {
        type: Number,
        require: true,
    },
    activitiesRate: {
        type: Number,
        require: true,
    },
    lectId: {
        type: String,
        require: true,
    }
});

module.exports = mongoose.model('Rate', rateSchema);
