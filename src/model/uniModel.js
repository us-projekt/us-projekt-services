const mongoose = require('mongoose');

const  uniSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        require: true,
        maxLength: 200
    },
    city: {
        type: String,
        require: true,
        maxLength: 200
    },
    type: {
        type: String,
        require: true,
        maxLength: 100,
    },
    isPublic: {
        type: String,
        require: true,
        maxLength: 20,
    }
});

module.exports = mongoose.model('Uni', uniSchema);
