const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        require: true,
        maxLength: 100
    },
    surname: {
        type: String,
        require: true,
        maxLength: 200
    },
    email: {
        type: String,
        require: true,
        maxLength: 200,
        unique: true
    },
    password: {
        type: String,
        require: true,
        maxLength: 20,
    },
    favouritesUniId: {
        type: [String],
        require: true,
    },
});

module.exports = mongoose.model('User', userSchema);
