require('dotenv').config();

const http = require('http');
const app = require('./app');
const port = process.env.PORT || 2020;


http.createServer(/*httpsOptions,*/ app)
    .listen(port, () => {
        console.log(`[Server] Server is running at https://localhost:${port}`);
    });
