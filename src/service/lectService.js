const mongoose = require('mongoose');
const Lect = require('../model/lectModel');
const Uni = require('../model/uniModel');
const nodeMailer = require('nodemailer');

exports.getAllLect = (req, res) => {
    const id = req.params.uniId;

    Uni.findById(id)
        .select('name')
        .exec()
        .then(result => {
            const name = result.name;
            Lect.find({uniId: id})
                .exec()
                .then(docs => {
                    const response = {
                        name,
                        count: docs.length,
                        lects: docs
                    };
                    res.status(200).json(response);
                })
                .catch(err => {
                    res.status(500).json({
                        code: 500,
                        error: err
                    });
                })
        });

};



exports.getLect = (req, res) => {
    const id = req.params.lectId;

    Lect.findById(id)
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                lect: docs
            };
            res.status(200).json(response);
        })
        .catch(err => {
            res.status(500).json({
                code: 500,
                error: err
            });
        })
};

exports.patchSummaryRate = (req, res) => {
        const lectId = req.params.lectId;
        const updateOps = {};
        console.log(lectId);
        for (const ops of req.body) {
            updateOps[ops.propName] = ops.value
        }
        Lect.update({ _id: lectId }, { $set: updateOps })
            .exec()
            .then(result => {
                console.log(result);
                res.status(200).json(result);
            })
            .catch(err => {
                console.log(err);
                res.status(500).json({
                    error: err
                });
            });
}

exports.addRequest = (req, res) => {
    const params = req.body;

    const lect = {
        name: params.name,
        surname: params.surname,
        faculty: params.faculty,
        uniId: params.uniId,
    }

    const transporter = nodeMailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'twojprofesor.pomoc@gmail.com',
            pass: 'twojprofesor123',
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    const options = {
        from: 'twojprofesor.pomoc@gmail.com',
        to: 'twojprofesor.pomoc@gmail.com',
        subject: `Prośba o dodanie prowadzącego - ${lect.name} ${lect.surname}`,
        text: `Nowa prośba o dodanie prowadzącego o następujących danych:
               ID Autora: ${params.authorId}
               ID Uczelni: ${lect.uniId},
               Imię: ${lect.name},
               Nazwisko: ${lect.surname},
               Wydział: ${lect.faculty}`
    }

    transporter.sendMail(options, (error, info) => {
        if (error) {
            console.log(error)
        } else {
            console.log('Email sent: ' + info.response)
        }
    })

    res.status(200).json('sent mail');

};
