const mongoose = require('mongoose');
const Rate = require('../model/rateModel');

exports.getRates = (req, res) => {
    const id = req.params.lectId;
    console.log(id);
    Rate.find({lectId: id})
        .select("_id author description date summaryRate lectureRate competenceRate passRate proStudentRate activitiesRate")
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                rates: docs
            };
            res.status(200).json(response);
        })
        .catch(err => {
            res.status(500).json({
                code: 500,
                error: err
            });
        })
};

exports.addRate = (req, res) => {
    const rate = new Rate({
        _id: new mongoose.Types.ObjectId(),
        author: req.body.author,
        userId: req.body.userId,
        description: req.body.description,
        date: req.body.date,
        summaryRate: req.body.summaryRate,
        lectureRate: req.body.lectureRate,
        competenceRate: req.body.competenceRate,
        passRate: req.body.passRate,
        proStudentRate: req.body.proStudentRate,
        activitiesRate: req.body.activitiesRate,
        lectId: req.body.lectId
    });

    rate.save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                massage: 'Created rate',
                createdProduct: rate
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            });
        });
};

exports.deleteRate = (req, res) => {
    Rate.remove({_id: req.params._id})
        .exec()
        .then(result => {
            res.status(200).json(result)
        })
        .catch(err => {
            res.status(500).json({error: err})
        })
};

exports.checkUserId = (req, res) => {
    const userId = req.params.userId;
    const commentId = req.params.commentId;
    let comment;
    console.log(userId);
    console.log(commentId);
    Rate.findOne({_id: commentId})
        .exec()
        .then(
            docs => {
                if (docs.userId === userId) {
                    res.status(200).json({userId: true});
                } else {
                    res.status(200).json({userId: false});
                }
            })
        .catch(err => {
            res.status(500).json({
                code: 500,
                error: err
            });
        })
}

exports.isDoubledComment = (req, res) => {
    const userId = req.params.userId;
    const lectId = req.params.lectId;
    let comment;
    console.log(userId);
    Rate.find({lectId: lectId})
        .exec()
        .then(
            docs => {
                for (let x of docs) {
                    if (userId === x.userId) {
                        res.status(200).json({status: true});
                    }
                }
                res.status(200).json({status: false});
            })
        .catch(err => {
            res.status(500).json({
                code: 500,
                error: err
            });
        })
};
