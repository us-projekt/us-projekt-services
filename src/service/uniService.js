const mongoose = require('mongoose');
const Uni = require('../model/uniModel');
const User = require('../model/userModel');

const idArray = [
    '5eac87d93459af617049c19c',
    '5eac890090d4069fab601282',
    '5eac89ff90d4069fab601283',
    '5eac8b10896221732212baf2'
];


exports.getAllUnis = (req, res) => {
    Uni.find()
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                unis: docs
            };
            res.status(200).json(response);
        })
        .catch(err => {
            res.status(500).json({
                code: 500,
                error: err
            });
        })
};


exports.getTopUnisId = (req, res) => {
    Uni.find({_id: idArray})
        .select('_id name')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                ids: docs
            };
            res.status(200).json(response);
        })
        .catch(err => {
            res.status(500).json({
                code: 500,
                error: err
            });
        })
};

exports.getFavUnis = (req, res) => {
    const id = req.params.userId;
    User.findById(id)
        .select('favouritesUniId')
        .exec()
        .then(uniIds => {
            Uni.find({_id: uniIds.favouritesUniId})
                .select('_id name city type isPublic')
                .exec()
                .then(docs => {
                    const response = {
                        count: docs.length,
                        unis: docs
                    };
                    res.status(200).json(response);
                })
                .catch(err => {
                    res.status(500).json({
                        code: 500,
                        error: err
                    });
                })
        })
        .catch(err => {
            res.status(500).json({
                code: 500,
                error: err
            });
        })
};


exports.setFavUnis = (req, res) => {
    const updateOps = {};
    updateOps.favouritesUniId = req.body.favouritesUniId;

    User.update({_id: req.params.userId}, {$set: updateOps})
        .exec()
        .then(result => {
            console.log(result);
            res.status(200).json(result)
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({error: err})
        })

};

