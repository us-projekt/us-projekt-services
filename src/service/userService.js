const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcr = require('bcrypt');

const User = require('../model/userModel');

exports.signUp = (req, res) => {
    User.find({email: req.body.email})
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.status(422).json({
                    message: 'User with this email address already exists'
                })
            } else {
                bcr.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.status(500).json({
                            error: err
                        });
                    } else {
                        const user = new User({
                            _id: mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash,
                            name: req.body.name,
                            surname: req.body.surname,
                            favouritesUniId: []
                        });
                        const token = jwt.sign({
                                _id: user._id,
                                name: user.name,
                                surname: user.surname,
                            },
                            process.env.JWT_KEY);
                        user.save()
                            .then(result => {
                                return res.status(201).json({
                                    code: 201,
                                    token: token,
                                    message: 'User has been created successfully',
                                    user: result.email
                                })
                            })
                            .catch(err => {
                                return res.status(500).json({
                                    error: err,
                                    code: 500
                                })
                            })
                    }
                })
            }
        })
};

exports.logIn = (req, res) => {
    User.find({email: req.body.email})
        .exec()
        .then(user => {
            if (user.length < 1) {
                return res.status(401).json({
                    massage: "Wprowadzono nieprawidłowy email lub hasło"
                });
            }
            bcr.compare(req.body.password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(401).json({
                        massage: "Wprowadzono nieprawidłowy email lub hasło"
                    });
                }
                if (result) {
                    const token = jwt.sign({
                            _id: user[0]._id,
                            name: user[0].name,
                            surname: user[0].surname,
                        },
                        process.env.JWT_KEY);
                    return res.status(200).json({
                        massage: 'Authorization successful',
                        token: token,
                        expiresIn: "1h"
                    });
                }
                return res.status(401).json({
                    massage: "Wprowadzono nieprawidłowy email lub hasło"
                });
            })
        })
        .catch()
};
